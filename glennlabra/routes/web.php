<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//auto redirect to port:8000
Route::get('/','MovieController@getList');
//view movie list route
Route::get('/movie/list','MovieController@getList');
//view individual movie details
Route::get('/movie/view/{mov_id}','MovieController@getDetails');