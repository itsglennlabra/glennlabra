@extends('movieIndex')

@section('content')
<section class="row" style="margin: 0; padding-top: 20px; padding-bottom: 20px;">
    <section class="col-sm-1"></section>
    <section class="col-sm-12">
    <div>
        <h1 class="text-center">Cine Oriente</h1>
        <h3 class="text-center">Database</h3><br>
    </div>
        <div class="text-center">
        <table class="table">
            <div>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Year Released</th>
                    <th>Length</th>
                    <th>Language</th>
                    <th>Date Released</th>
                    <th>Country Origin</th>
                    <th></th>
                </tr>
            </div>
        </div>
            <div>
            @foreach($allMovies as $movie)
                <tr>
                    <td>{{ $movie->mov_id }}</td>
                    <td>{{ $movie->mov_title }}</td>
                    <td>{{ $movie->mov_year }}</td>
                    <td>{{ $movie->mov_time }}</td>
                    <td>{{ $movie->mov_lang }}</td>
                    <td>{{ $movie->mov_dt_rel }}</td>
                    <td>{{ $movie->mov_rel_country }}</td>
                    <td><a href="{{ url('/movie/view/'.$movie->mov_id) }}" class="btn btn-success">Movie Details</a></td>
                </tr>
            @endforeach
            </div>
        </table>
    </section>
</section>

<section class="row" style="margin:0;">
    <section class="col-sm-12 text-center" style="display:flex; justify-content:center;">
        {{ $allMovies->links() }}
    </section>
</section>

@endsection