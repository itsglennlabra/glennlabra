@extends('movieIndex')

@section('content')
<section class="row" style="margin: 0; padding-top: 20px; padding-bottom: 20px;">

    <section class="col-sm-12">
    <br>
    <div>
        <h1 class="text-center">Cine Oriente</h1>
        <h3 class="text-center">Database</h3><br>
    </div>
    <h1 class="text-center">Movie Information</h1>
        <table class="table">
            <tr class="text-left">
            <td >Movie Title: </td>
            <td><b>{{$movie->mov_title}}</b></td>
            </tr>
            
            <tr class="text-left">
            <td>Year: </td>
            <td>{{$movie->mov_year}}</td>
            </tr>

            <tr class="text-left">
            <td>Running Time: </td>
            <td>{{$movie->mov_time}} minutes</td>
            </tr>

            <tr class="text-left">
            <td>Directed By: </td>
            <td>
                @foreach($movie->connectDirectors as $director)
                {{ $director->dir_fname }}
                {{ $director->dir_lname }}
                @endforeach
            </td>
            </tr>

            <tr class="text-left">
            <td>Starring: </td>
            <td>
                @foreach ($movie->connectActors as $actor)
                {{ $actor->act_fname }} {{ $actor->act_lname }}
                @endforeach 
                ,&nbsp; 
                @foreach ($movie->connectActors as $role)
                {{ $role->pivot->role }}
                @endforeach 
            </td>
            </tr>

            <tr class="text-left">
            <td>Genre: </td>
            <td>
                @foreach ($movie->connectGenres as $genre)
                {{ $genre->gen_title }}
                @endforeach
            </td>
            </tr>

            <tr class="text-left">
            <td>Rating: </td>
            <td>
                @foreach ($movie->connectReviewers as $reviewer)
                {{ $reviewer->rev_name }}
                @endforeach
            </td>
            </tr>

            <tr class="text-left">
            <td>Score: </td>
            <td>
                @foreach ($movie->connectReviewers as $star)
                {{ $star->pivot->rev_stars }}
                @endforeach stars
            </td>
            </tr>
        </table>
    </section>
</section>

<section class="row" style="margin:0;">
    <section class="col-sm-12 text-center" style="display:flex; justify-content:center;">
        <a href="{{ url('/movie/list') }}" class="btn btn-danger">Go Back to List</a>
    </section>
</section>

@endsection