<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $foreign_rev_id = 'rev_id'; 
    protected $foreign_mov_id = 'mov_id';

    protected $fillable = [
        'mov_id',
        'rev_id',
        'rev_stars',
        'num_o_ratings'
    ];
}
