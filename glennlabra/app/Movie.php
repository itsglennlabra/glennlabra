<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    public $table = 'movie';
    protected $primaryKey = 'mov_id';

    public function connectActors(){
        return $this->belongsToMany('App\Actor', 'movie_cast', 'mov_id', 'act_id')->withPivot('role');
    }
    
    public function connectDirectors(){
        return $this->belongsToMany('App\Director', 'movie_direction', 'mov_id', 'dir_id');
    }

    public function connectGenres(){
        return $this->belongsToMany('App\Genres', 'movie_genres', 'mov_id', 'gen_id');
    }

    public function connectReviewers(){
        return $this->belongsToMany('App\Reviewer', 'rating', 'mov_id', 'rev_id')->withPivot('rev_stars');
    }

    public static function getMovieDetails($mov_id){
        return self::find($mov_id);
    }

    public static function getAllMovies(){
        return self::paginate(10);
    }
}
