<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie_Genre extends Model
{
    protected $foreign_gen_id = 'gen_id'; 
    protected $foreign_mov_id = 'mov_id';

    protected $fillable = [
        'mov_id',
        'gen_id'
    ];
}
