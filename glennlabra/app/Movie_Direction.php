<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie_Direction extends Model
{
    protected $foreign_dir_id = 'dir_id';
    protected $foreign_mov_id = 'mov_id';

    protected $fillable = [
        'dir_id',
        'move_id'
    ];
}
