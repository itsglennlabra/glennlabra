<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    public $table = 'genres';
    protected $primaryKey = 'gen_id';

    public function connectMovies(){
        return $this->hasMany('App\Movie', 'movie_genres', 'gen_id', 'mov_id');
    }
}
