<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Director;
use App\Movie_Direction;

class MovieController extends Controller
{
    //generate movie list
    public function getList(){
        $allMovies = Movie::getAllMovies();
        return view('movieList',compact('allMovies'));
    }
    //get individual movie details
    public function getDetails(Request $request){
        $movie = Movie::getMovieDetails($request->mov_id);
        return view('movieView',compact('movie'));
    }
}
