<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie_Cast extends Model
{
    protected $foreign_act_id = 'act_id';
    protected $foreign_mov_id = 'mov_id';

    protected $fillable = [
        'act_id',
        'mov_id',
        'role'
    ];
}
