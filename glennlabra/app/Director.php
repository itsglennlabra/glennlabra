<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    public $table = 'director';
    protected $primaryKey = 'dir_id';

    public function connectMovies(){
        return $this->hasMany('App\Movie', 'movie_direction', 'dir_id', 'mov_id');
    }
}
